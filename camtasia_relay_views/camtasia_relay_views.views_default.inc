<?php
// $Id$
/**
 * @file
 * Contains default views on behalf of the draft_views module.
 */

/**
 * Implementation of hook_default_view_views().
 */
function camtasia_relay_views_views_default_views() {
$view = new view;
$view->name = 'camtasia_relay_metrics';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Camtasia Relay Metrics';
$view->core = 7;
$view->api_version = '3.0-alpha1';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Camtasia Relay Metrics';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = 'camtasia_relay_profile';
$handler->display->display_options['style_options']['columns'] = array(
  'camtasia_relay_date' => 'camtasia_relay_date',
  'camtasia_relay_duration' => 'camtasia_relay_duration',
  'camtasia_relay_presenter_email' => 'camtasia_relay_presenter_email',
  'camtasia_relay_presenter_name' => 'camtasia_relay_presenter_name',
  'camtasia_relay_profile' => 'camtasia_relay_profile',
  'title' => 'title',
  'totalcount' => 'totalcount',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'camtasia_relay_date' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'camtasia_relay_duration' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'camtasia_relay_presenter_email' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'camtasia_relay_presenter_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'camtasia_relay_profile' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'totalcount' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['empty_table'] = 0;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = FALSE;
$handler->display->display_options['empty']['area']['content'] = 'No Presentations Yet';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_label_colon'] = 0;
$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = 'Description';
$handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['body']['alter']['external'] = 0;
$handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['body']['alter']['trim'] = 0;
$handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['body']['alter']['html'] = 0;
$handler->display->display_options['fields']['body']['element_label_colon'] = 0;
$handler->display->display_options['fields']['body']['element_default_classes'] = 1;
$handler->display->display_options['fields']['body']['hide_empty'] = 0;
$handler->display->display_options['fields']['body']['empty_zero'] = 0;
$handler->display->display_options['fields']['body']['field_api_classes'] = 0;
/* Field: Content: Date */
$handler->display->display_options['fields']['camtasia_relay_date']['id'] = 'camtasia_relay_date';
$handler->display->display_options['fields']['camtasia_relay_date']['table'] = 'field_data_camtasia_relay_date';
$handler->display->display_options['fields']['camtasia_relay_date']['field'] = 'camtasia_relay_date';
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['external'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['trim'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['alter']['html'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['element_label_colon'] = 1;
$handler->display->display_options['fields']['camtasia_relay_date']['element_default_classes'] = 1;
$handler->display->display_options['fields']['camtasia_relay_date']['hide_empty'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['empty_zero'] = 0;
$handler->display->display_options['fields']['camtasia_relay_date']['field_api_classes'] = 0;
/* Field: Content: Profile */
$handler->display->display_options['fields']['camtasia_relay_profile']['id'] = 'camtasia_relay_profile';
$handler->display->display_options['fields']['camtasia_relay_profile']['table'] = 'field_data_camtasia_relay_profile';
$handler->display->display_options['fields']['camtasia_relay_profile']['field'] = 'camtasia_relay_profile';
$handler->display->display_options['fields']['camtasia_relay_profile']['exclude'] = TRUE;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['external'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['trim'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['alter']['html'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['element_label_colon'] = 1;
$handler->display->display_options['fields']['camtasia_relay_profile']['element_default_classes'] = 1;
$handler->display->display_options['fields']['camtasia_relay_profile']['hide_empty'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['empty_zero'] = 0;
$handler->display->display_options['fields']['camtasia_relay_profile']['field_api_classes'] = 0;
/* Field: Content: Duration */
$handler->display->display_options['fields']['camtasia_relay_duration']['id'] = 'camtasia_relay_duration';
$handler->display->display_options['fields']['camtasia_relay_duration']['table'] = 'field_data_camtasia_relay_duration';
$handler->display->display_options['fields']['camtasia_relay_duration']['field'] = 'camtasia_relay_duration';
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['external'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['trim'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['alter']['html'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['element_label_colon'] = 1;
$handler->display->display_options['fields']['camtasia_relay_duration']['element_default_classes'] = 1;
$handler->display->display_options['fields']['camtasia_relay_duration']['hide_empty'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['empty_zero'] = 0;
$handler->display->display_options['fields']['camtasia_relay_duration']['field_api_classes'] = 0;
/* Field: Content: Presenter Email */
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['id'] = 'camtasia_relay_presenter_email';
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['table'] = 'field_data_camtasia_relay_presenter_email';
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['field'] = 'camtasia_relay_presenter_email';
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['external'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['trim'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['alter']['html'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['element_label_colon'] = 1;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['element_default_classes'] = 1;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['hide_empty'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['empty_zero'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_email']['field_api_classes'] = 0;
/* Field: Content: Presenter Name */
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['id'] = 'camtasia_relay_presenter_name';
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['table'] = 'field_data_camtasia_relay_presenter_name';
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['field'] = 'camtasia_relay_presenter_name';
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['external'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['trim'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['alter']['html'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['element_label_colon'] = 1;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['element_default_classes'] = 1;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['hide_empty'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['empty_zero'] = 0;
$handler->display->display_options['fields']['camtasia_relay_presenter_name']['field_api_classes'] = 0;
/* Field: Content statistics: Total views */
$handler->display->display_options['fields']['totalcount']['id'] = 'totalcount';
$handler->display->display_options['fields']['totalcount']['table'] = 'node_counter';
$handler->display->display_options['fields']['totalcount']['field'] = 'totalcount';
$handler->display->display_options['fields']['totalcount']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['totalcount']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['totalcount']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['totalcount']['alter']['external'] = 0;
$handler->display->display_options['fields']['totalcount']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['totalcount']['alter']['trim'] = 0;
$handler->display->display_options['fields']['totalcount']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['totalcount']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['totalcount']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['totalcount']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['totalcount']['alter']['html'] = 0;
$handler->display->display_options['fields']['totalcount']['element_label_colon'] = 1;
$handler->display->display_options['fields']['totalcount']['element_default_classes'] = 1;
$handler->display->display_options['fields']['totalcount']['hide_empty'] = 0;
$handler->display->display_options['fields']['totalcount']['empty_zero'] = 0;
$handler->display->display_options['fields']['totalcount']['format_plural'] = 0;
/* Field: Taxonomy: All terms */
$handler->display->display_options['fields']['tid']['id'] = 'tid';
$handler->display->display_options['fields']['tid']['table'] = 'taxonomy_index';
$handler->display->display_options['fields']['tid']['field'] = 'tid';
$handler->display->display_options['fields']['tid']['label'] = 'Tags';
$handler->display->display_options['fields']['tid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['tid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['tid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['tid']['alter']['external'] = 0;
$handler->display->display_options['fields']['tid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['tid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['tid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['tid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['tid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['tid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['tid']['alter']['html'] = 0;
$handler->display->display_options['fields']['tid']['element_label_colon'] = 0;
$handler->display->display_options['fields']['tid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['tid']['hide_empty'] = 0;
$handler->display->display_options['fields']['tid']['empty_zero'] = 0;
$handler->display->display_options['fields']['tid']['separator'] = ',';
$handler->display->display_options['fields']['tid']['link_to_taxonomy'] = 1;
$handler->display->display_options['fields']['tid']['limit'] = 0;
$handler->display->display_options['fields']['tid']['vocabularies'] = array(
  'courseid' => 0,
  'courseid1' => 0,
  'courseid2' => 0,
  'courseid3' => 0,
  'courseid4' => 0,
  'courseid5' => 0,
  'department' => 0,
  'department1' => 0,
  'department2' => 0,
  'department3' => 0,
  'department4' => 0,
  'department5' => 0,
  'departmsdfent4' => 0,
  'sdfsd' => 0,
  'user_vocab' => 0,
  'profile_vocabulary' => 0,
  'tags' => 0,
);
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'camtasia_relay' => 'camtasia_relay',
);
/* Filter criterion: Content: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'node';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['operator'] = 'contains';
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Search Presentation Title';
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
$handler->display->display_options['filters']['title']['expose']['required'] = FALSE;
$handler->display->display_options['filters']['title']['expose']['remember'] = 1;
$handler->display->display_options['filters']['title']['expose']['multiple'] = FALSE;
/* Filter criterion: Taxonomy: Term */
$handler->display->display_options['filters']['name']['id'] = 'name';
$handler->display->display_options['filters']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['filters']['name']['field'] = 'name';
$handler->display->display_options['filters']['name']['operator'] = 'allwords';
$handler->display->display_options['filters']['name']['exposed'] = TRUE;
$handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['label'] = 'Tags';
$handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
$handler->display->display_options['filters']['name']['expose']['required'] = FALSE;
$handler->display->display_options['filters']['name']['expose']['remember'] = 1;
$handler->display->display_options['filters']['name']['expose']['multiple'] = FALSE;
/* Filter criterion: User: Current */
$handler->display->display_options['filters']['uid_current']['id'] = 'uid_current';
$handler->display->display_options['filters']['uid_current']['table'] = 'users';
$handler->display->display_options['filters']['uid_current']['field'] = 'uid_current';

/* Display: all-Page */
$handler = $view->new_display('page', 'all-Page', 'page');
$handler->display->display_options['path'] = 'camtasia-relay-metrics';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Camtasia Relay Metrics';
$handler->display->display_options['menu']['weight'] = '0';

/* Display: my-Page */
$handler = $view->new_display('page', 'my-Page', 'page_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'My Presentations';
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'camtasia_relay' => 'camtasia_relay',
);
/* Filter criterion: Content: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'node';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['operator'] = 'contains';
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Search Presentation Title';
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
$handler->display->display_options['filters']['title']['expose']['required'] = FALSE;
$handler->display->display_options['filters']['title']['expose']['remember'] = 1;
$handler->display->display_options['filters']['title']['expose']['multiple'] = FALSE;
/* Filter criterion: Taxonomy: Term */
$handler->display->display_options['filters']['name']['id'] = 'name';
$handler->display->display_options['filters']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['filters']['name']['field'] = 'name';
$handler->display->display_options['filters']['name']['operator'] = 'allwords';
$handler->display->display_options['filters']['name']['exposed'] = TRUE;
$handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['label'] = 'Tags';
$handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
$handler->display->display_options['filters']['name']['expose']['required'] = FALSE;
$handler->display->display_options['filters']['name']['expose']['remember'] = 1;
$handler->display->display_options['filters']['name']['expose']['multiple'] = FALSE;
/* Filter criterion: User: Current */
$handler->display->display_options['filters']['uid_current']['id'] = 'uid_current';
$handler->display->display_options['filters']['uid_current']['table'] = 'users';
$handler->display->display_options['filters']['uid_current']['field'] = 'uid_current';
$handler->display->display_options['filters']['uid_current']['value'] = '1';
$handler->display->display_options['path'] = 'camtasia-relay/my';
$translatables['camtasia_relay_metrics'] = array(
  t('Master'),
  t('Camtasia Relay Metrics'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('No Presentations Yet'),
  t('Title'),
  t('Description'),
  t('Date'),
  t('Profile'),
  t('Duration'),
  t('Presenter Email'),
  t('Presenter Name'),
  t('Total views'),
  t('.'),
  t(','),
  t('Tags'),
  t('Search Presentation Title'),
  t('all-Page'),
  t('my-Page'),
  t('My Presentations'),
);




$views[$view->name] = $view;

return $views;
}
