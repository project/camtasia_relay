<?php

/**
 * @file
 * View callbacks for the camtasia_relay module.
 */
 
/**
 * Implementation of hook_views_data().
 *
 * @return array
 */
function camtasia_relay_views_data() {
  $data['camtasia_relay']['table']['group'] = t('Camtasia Relay');

  $data['camtasia_relay']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['camtasia_relay']['title'] = array(
    'title' => t('Title'),
    'help' => t('The Title of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['camtasia_relay']['body'] = array(
    'title' => t('Description'),
    'help' => t('The Description of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['camtasia_relay']['date'] = array(
    'title' => t('Date'),
    'help' => t('The Date of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['profile'] = array(
    'title' => t('Profile'),
    'help' => t('The Profile of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['display_name'] = array(
    'title' => t('Display Name'),
    'help' => t('The Display Name of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['user_name'] = array(
    'title' => t('User Name'),
    'help' => t('The User Name of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['email'] = array(
    'title' => t('Email'),
    'help' => t('The Email of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['client_ip'] = array(
    'title' => t('Client IP'),
    'help' => t('The Client IP of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['client_computer_name'] = array(
    'title' => t('Client Computer Name'),
    'help' => t('The Client Computer Name of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['server_host_name'] = array(
    'title' => t('Server Host Name'),
    'help' => t('The Server Host Name of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['encoding_preset'] = array(
    'title' => t('Encoding Preset'),
    'help' => t('The Encoding Preset of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['time_to_encode'] = array(
    'title' => t('Time To Encode'),
    'help' => t('The Time To Encode of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['time_in_queue'] = array(
    'title' => t('Time In Queue'),
    'help' => t('The Time In Queue of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['source_resolution'] = array(
    'title' => t('Source Resolution'),
    'help' => t('The Source Resolution of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'help' => t('Filter the content based on Source Resolution.'),
      'label' => 'camtasia_relay Source Resolution',
    ),
  );

  $data['camtasia_relay']['source_duration'] = array(
    'title' => t('Source Duration'),
    'help' => t('The Source Duration of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'help' => t('Filter the content based on Source Duration.'),
      'label' => 'camtasia_relay Source Duration',
    ),
  );

  $data['camtasia_relay']['source_file'] = array(
    'title' => t('Source File'),
    'help' => t('The Source File of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['camtasia_relay']['output_resolution'] = array(
    'title' => t('Output Resolution'),
    'help' => t('The Output Resolution of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'help' => t('Filter the content based on Output Resolution.'),
      'label' => 'camtasia_relay Output Resolution',
    ),
  );

  $data['camtasia_relay']['output_duration'] = array(
    'title' => t('Output Duration'),
    'help' => t('The Output Duration of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'help' => t('Filter the content based on Output Duration.'),
      'label' => 'camtasia_relay Output Duration',
    ),
  );
  $data['camtasia_relay']['output_file'] = array(
    'title' => t('Output File'),
    'help' => t('The Output File of the media.'),
    // Information for displaying the field
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  return $data;
}