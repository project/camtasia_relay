<?php

/**
 * @file
 * This is a Camtasia Relay Presentation module
 */

/**
 * camtasia_relay_meta_access function
 * returns 1 if the current user has access to meta functions.
 * @param string $nid
 * @return 1/0
 */
function camtasia_relay_meta_access($node) {
  global $user;
  if (((user_access('view meta information'))&&($node->type == 'camtasia_relay')&&($node->uid == $user->uid))||(user_access('administer nodes')))  {
    return 1;
  }
  else {
    return 0;
  }
}

/**
 * Implements hook_menu().
 *
 * We are providing a default page to illustrate the use of our custom node view
 * mode that will live at http://example.com/?q=examples/camtasia_relay
 */
function camtasia_relay_menu() {
$items['node/%node/camtasia-relay-meta'] = array(
    'title' => 'Meta Information',
    'page callback' => 'camtasia_relay_meta_info',
    'page arguments' => array(1),
    'access callback' => 'camtasia_relay_meta_access',
    'access arguments' => array(1),
    'weight' => 5,
    'type' => MENU_LOCAL_TASK,
  );
  $items['node/%node/camtasia-relay-download'] = array(
    'title' => 'Download',
    'page callback' => 'camtasia_relay_download_tab',
    'page arguments' => array(1),
    'access callback' => 'camtasia_relay_download_access',
    'access arguments' => array(1),
    'weight' => 5,
    'type' => MENU_LOCAL_TASK,
  );
    $items['admin/config/camtasia-relay'] = array(
    'title' => 'Camtasia Relay Configuration',
    'description' => 'Configure the Camtasia Relay Module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('camtasia_relay_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'camtasia_relay.admin.inc',
  );
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function camtasia_relay_permission() {
  return array(
    'create presentation' => array(
      'title' => t('Create Presentation'), 
      'description' => t('Create Camtasia Relay Presentation'),
    ),
    'edit own presentation' => array(
      'title' => t('Edit Own Presentation'), 
      'description' => t('Edit Own Camtasia Relay Presentation'),
    ),
    'edit any presentation' => array(
      'title' => t('Edit Any Presentation'), 
      'description' => t('Edit Any Camtasia Relay Presentation'),
    ),
    'delete own presentation' => array(
      'title' => t('Delete Own Presentation'), 
      'description' => t('Delete Own Camtasia Relay Presentation'),
    ),
    'delete any presentation' => array(
      'title' => t('Delete Any Presentation'), 
      'description' => t('Delete Any Camtasia Relay Presentation'),
    ),
    'show presentation download tab' => array(
      'title' => t('Show Presentation Download Tab'), 
      'description' => t('Show Presentation Download Tab'),
    ),
    'download own camrec file' => array(
      'title' => t('Download Own Camrec File'), 
      'description' => t('Download Own Camrec File'),
    ),
    'download any camrec file' => array(
      'title' => t('Download Any Camrec File'), 
      'description' => t('Download Any Camrec File'),
    ),
  );
}

/**
 * Implementation of hook_access().
 */
function camtasia_relay_node_access($node, $op, $account) {
  switch ($op) {
    case 'create':
      // Allow if user's role has 'create presentation' permission.
      return user_access('create presentation', $account);
      
    case 'update':
      // Allow if user's role has 'edit own presentation' permission and user is
      // the author; or if the user's role has 'edit any presentation' permission.
			global $user;
			$is_author = $user->uid == $node->uid;
      return user_access('edit own presentation', $account) && $is_author || user_access('edit any presentation', $account);
      
    case 'delete':
      // Allow if user's role has 'delete own presentation' permission and user is
      // the author; or if the user's role has 'delete any presentation' permission.
			global $user;
			$is_author = $user->uid == $node->uid;
      return user_access('delete own presentation', $account) && $is_author || user_access('delete any presentation', $account);
  }
}

/**
 * camtasia_relay_download_access function
 * returns 1 if the current user has access to download tab.
 * @param string $nid
 * @return 1/0
 */
function camtasia_relay_download_access($node) {
  if ($node->type == 'camtasia_relay')  {
    return (user_access('show presentation download tab') );
  }
  else {
    return 0;
  }
}

/**
 * Menu callback.
 * Called when user goes to http://example.com/?q=node/%node/camtasia_relay-meta
 */
function camtasia_relay_meta_info($node) {
 $output = "";
 $output .= "<b>Title: </b>".$node->title;
 $output .= "<br/><b>Date: </b>".$node->camtasia_relay_date['und'][0]['value'];
 $output .= "<br/><b>Profile: </b>".$node->camtasia_relay_profile['und'][0]['value'];
 $output .= "<br/><b>Duration: </b>".$node->camtasia_relay_duration['und'][0]['value']." Sec";
 $output .= "<br/><b>Presenter Name: </b>".$node->camtasia_relay_presenter_name['und'][0]['value'];
 $output .= "<br/><b>Presenter Email: </b>".$node->camtasia_relay_presenter_email['und'][0]['value'];
 if($node->camtasia_relay_recorder_email['und'][0]['value'] != $node->camtasia_relay_presenter_email['und'][0]['value']){
 $output .= "<br/><b>Recorder Name: </b>".$node->camtasia_relay_recorder_name['und'][0]['value'];
 $output .= "<br/><b>Recorder Email: </b>".$node->camtasia_relay_recorder_email['und'][0]['value'];
 }
  return $output;
}

/**
 * Menu callback.
 * Called when user goes to http://example.com/?q=node/%node/camtasia_relay-download
 */
function camtasia_relay_download_tab($node) {

  $header = array(t('Attachment'), t('Size'));
  $rows = array();
  foreach ($node->camtasia_relay_files['und'] as $delta => $item) {
    $rows[] = array(
      theme('file_link', array('file' => (object) $item)),
      format_size($item['filesize']),
    );
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  
  return $output;
}

/**
 * Implements hook_help().
 */
function camtasia_relay_help($path, $arg) {
  switch ($path) {
    case 'examples/camtasia_relay':
      return "<p>" . t(
        "The Camtasia Relay module provides a custom node type.
        You can create new nodes using the <a href='!nodeadd'>node add form</a>.
        Nodes that you create will be displayed here.",
        array('!nodeadd' => url('node/add/node-example'))
      ) . "</p>";
  }
}

/**
 * A custom theme function.
 *
 * By using this function to format our node-specific information, themes
 * can override this presentation if they wish.  This is a simplifed theme
 * function purely for illustrative purposes.
 */
function theme_camtasia_relay_node_date($variables) {
  $output = '<span style="background-date: #ccc; padding: 1em; margin-bottom: 1em; float: left; date: ' . $variables['date'] . '">' . $variables['date'] . '</span>';
  return $output;
}

/**
 * Implements hook_xmlrpc().
 *
 * @see hook_xmlrpc()
 */
function camtasia_relay_xmlrpc() {
$methods = array ( 'camtasia_relay_process_file' => _camtasia_relay_process_file ) ;
  return $methods ;
}

function _camtasia_relay_process_file($hash_checksum, $xml_file_path, $terms_array = NULL) {
	$log_msg = "RPC call made: camtasia_relay_process_file with parameters hash_checksum: $hash_checksum, xml_file_path: $xml_file_path";
	foreach ($terms_array as $vocabulary_name => $term_name) { 
		$log_msg .= "Term: $vocabulary_name => $term_name ; "; 
	}
	watchdog('camtasia_relay', $log_msg , NULL, WATCHDOG_ERROR, NULL);
  $flag_node_update = FALSE;

  $dirloc = variable_get('camtasia_relay_dirloc', '');

  $file_path = $dirloc."/".$xml_file_path;
  if (!is_file($file_path)) {
		$log_msg = "RPC call return: camtasia_relay_process_file File Not Exists";
		watchdog('camtasia_relay', $log_msg , NULL, WATCHDOG_ERROR, NULL);
    return "File Not Exists";
  }
  $xml = file_get_contents($file_path);

  //this creates the simple xml object

  $xmldata = new simplexmlelement($xml);
  $title = (string)$xmldata->title;
	$duration = (string)$xmldata->totalDuration;

  $record_time = (string)$xmldata->date;
  $stored_secret_key = (string)variable_get('camtasia_relay_shared_secret_key', '');
  $validation_checksum = camtasia_relay_check_sum_string( $stored_secret_key ) . camtasia_relay_check_sum_string( $duration ) . camtasia_relay_check_sum_string( $record_time ) . camtasia_relay_check_sum_string( $title );
  if($validation_checksum != $hash_checksum){
    $log_msg = "RPC call return: camtasia_relay_process_file validation_checksum: $validation_checksum";
		watchdog('camtasia_relay', $log_msg , NULL, WATCHDOG_ERROR, NULL);
    return "Access Denied";
  }
  $result = db_query('SELECT n.nid FROM {node} n WHERE n.type = '."'camtasia_relay'");

  
foreach ($result as $node) {
 
    $node_obj = node_load($node->nid);

    global $user;

    $user->uid = 1;
	if (($node_obj->title == $xmldata->title)&&($node_obj->camtasia_relay_profile['und'][0]['value'] == $xmldata->profile)&&($node_obj->camtasia_relay_presenter_email['und'][0]['value'] == $xmldata->presenter->email)&&($node_obj->camtasia_relay_date['und'][0]['value'] == $xmldata->date)) {

      $filesize = filesize($file_path);

      $des_file = camtasia_relay_file_move($file_path, $xmldata->presenter->displayName, $node_obj->camtasia_relay_profile['und'][0]['value']);
      camtasia_relay_file_attach($des_file, $filesize, $node_obj);

      $xml_files = $xmldata->outputFiles->fileList->file;

      foreach ($xml_files as $xml_file) {

        $output_file_name = $xml_file['name'];

        $output_file_path = dirname($file_path) ."/". $output_file_name;

        if (file_exists($output_file_path)) {
          $filesize = filesize($output_file_path);
					$des_file = camtasia_relay_file_move($output_file_path, $xmldata->presenter->displayName, $node_obj->camtasia_relay_profile['und'][0]['value']);
          camtasia_relay_file_attach($des_file, $filesize, $node_obj);
        }
      }

      $flag_node_update = TRUE;

      node_save($node_obj);

      return url("node/$node_obj->nid", array('absolute' => TRUE));

    }

  }

  

  if ($flag_node_update == FALSE) {
    $newnode = camtasia_relay_node_create($xmldata, $file_path, 1, 1, 0, 'camtasia_relay');
      if ($newnode==NULL) {
        return "Error in creating node";
      }
	//Relay3
	//no need to tag when updating node.
	$node_terms = array();
	foreach ($terms_array as $vocabulary_name => $term_name) { 
		$vocabulary_name = trim($vocabulary_name);
		$term_name = trim($term_name);
		$vid = NULL;
		$vocab_object = camtasia_relay_get_vocabulary_by_name($vocabulary_name);
		if($vocab_object == NULL){
			//Vocabulary doesn't exist. Create Vocabulary
			$vid = create_vocabulary($vocabulary_name);
		}else{
			$vid = $vocab_object->vid;
		}
		
		$node_terms[$term_name]=$vid;
	}
	$choice_add_terms = variable_get('camtasia_relay_add_terms', '0');
	if ($choice_add_terms==1)
	{
		attach_term2node($node_terms, $newnode);
	}
	node_save($newnode);

	return url("node/$newnode->nid", array('absolute' => TRUE));

  }

   return "Error in Content Creation";

}

function camtasia_relay_get_vocabulary_by_name($vocabulary_name) {
  $vocabs = taxonomy_get_vocabularies();
  foreach ($vocabs as $vocab_object) {
    if ($vocab_object->name == $vocabulary_name) {
      return $vocab_object;
    }
  }
  return NULL;
}

function create_vocabulary($vocabulary_name) {
$machine_name = strtolower($vocabulary_name);
$machine_name = trim($machine_name);
$machine_name = str_replace(" ","_",$machine_name);
	$edit = array(
      'name' => $vocabulary_name, 
      'machine_name' => $machine_name, 
      'description' => t('Camtasia Relay vocabulary'), 
      'hierarchy' => 1, 
      'module' => 'camtasia_relay', 
      'weight' => -10,
    );
    $vocabulary = (object) $edit;
    taxonomy_vocabulary_save($vocabulary);
	return $vocabulary->vid;
} 

function camtasia_relay_check_sum_string($input)
{
  $sum = 458289; // Starting Checksum Seed
  $multiplier = 3;
  $input_len = strlen($input);
  for ( $i = 0; $i < $input_len; $i++ )
  {
     $sum += ord((string)$input[$i]) * $multiplier;
     $multiplier++;
  }
  return $sum;
}

function camtasia_relay_file_attach( $file_path_des, $filesize, $node_obj) {
  $url = file_create_url($file_path_des);
  $name = basename($file_path_des);
  $mime = file_get_mimetype($name);
  $file = new stdClass();
  $file->filename = $name;
  $file->filepath = $file_path_des;
  $file->filemime = $mime;
  $file->filesize = $filesize;
 $f_uri = file_default_scheme() . '://' .$file_path_des;
  $file->uri = $f_uri;

  $file->uid = $node_obj->uid;
  $file->status = FILE_STATUS_PERMANENT;
  $file->timestamp = time();
  $file->list = 1;
  $file->new = TRUE;

 try
  {
  drupal_write_record('file_managed', $file);
  }
catch(Exception $e)
  {
  watchdog('camtasia_relay', 'file: '. $e->getMessage() , NULL, WATCHDOG_ERROR, NULL);
  return "";
  }
  // Attach the file object to your node
  $err_msg = $file->fid;
  watchdog('camtasia_relay', 'file $url '. $err_msg , NULL, WATCHDOG_ERROR, NULL);
  $node_obj->camtasia_relay_files['und'][] = 
  array(
      'fid' => $file->fid,
      'title' => basename($file->filename),
      'filename' => $file->filename,
      'filepath' => $file->filepath,
      'filesize' => $file->filesize,
	  'uri' => $f_uri,
      'filemime' => $mime,
	  'display'  => 1,
	  'uid'  => $file->uid,
	  'status'  => 1,
      'data' => array(
        'description' => basename($file->filename),
      ),
    );
}

function camtasia_relay_file_move($src_file_path, $user_name, $profile) {

  $user_name = str_ireplace(' ', '_', $user_name);

  $profile = str_ireplace(' ', '_', $profile);

  $info = pathinfo($src_file_path);

$files_dir = camtasia_relay_document_root() . base_path() . variable_get('file_public_path', conf_path() . '/files')  ."/";


  $camtasia_relay_dir = $files_dir . 'camtasia_relay1/';

  $user_dir = $camtasia_relay_dir . $user_name .'/';

  $profile_dir = $user_dir . $profile .'/';

  $ext_dir = $profile_dir;

  $des_file_path = $ext_dir . '/'. basename($src_file_path);
  camtasia_relay_mkdir_r($ext_dir);
 $ret =   file_unmanaged_copy($src_file_path, $des_file_path,  FILE_EXISTS_REPLACE);
  $relative_file_path = 'camtasia_relay1/' . $user_name .'/'. $profile . '/' . basename($src_file_path);
  return $relative_file_path;

}

/**
 * camtasia_relay_mkdir_r function
 * creates directories recursively
 * @param string $dirName
 * @param string $rights
 */
function camtasia_relay_mkdir_r($dir_name, $rights=0777) {
  $dirs = explode('/', $dir_name);
  $dir='';
  foreach ($dirs as $part) {
    $dir .=$part . '/';
    if (!is_dir($dir) && drupal_strlen($dir)>0)
      mkdir($dir, $rights);
  }
}

function camtasia_relay_node_create($xmldata, $xml_file_path, $uid, $status, $promote, $type) {
	$choice_add_terms = variable_get('camtasia_relay_add_terms', '0');
  $newnode = new stdClass();

  $newnode->title = $xmldata->title;

  if (!empty($xmldata->description)) {

    $newnode->body['und'][0]['value'] = $xmldata->description;

  }

  else {

    $newnode->body['und'][0]['value'] = 'Description Not Provided';

  }

  $newnode->camtasia_relay_date['und'][0]['value'] = $xmldata->date;

  $newnode->camtasia_relay_profile['und'][0]['value'] = $xmldata->profile;
  
  $newnode->camtasia_relay_duration['und'][0]['value'] = $xmldata->trimmedDuration;

  $newnode->camtasia_relay_presenter_name['und'][0]['value'] = $xmldata->presenter->displayName;

  $newnode->camtasia_relay_presenter_email['und'][0]['value'] = $xmldata->presenter->email;
  
  $newnode->camtasia_relay_recorder_name['und'][0]['value'] = $xmldata->recordedBy->displayName;

  $newnode->camtasia_relay_recorder_email['und'][0]['value'] = $xmldata->recordedBy->email;

  $camtasia_relay_default_user = variable_get('camtasia_relay_default_user', '');

  $mail_Opt = variable_get('camtasia_relay_node_mail', '');



  if (empty($camtasia_relay_default_user)) {

    $user_name = $xmldata->presenter->displayName;

  }

  else {

    $user_name = $camtasia_relay_default_user;

  }

  $uid = camtasia_relay_user_check($user_name, $xmldata->presenter->email, $mail_Opt);
  global $user;

  $user = user_load($uid);

  $newnode->uid = $uid;

  $filesize = filesize($xml_file_path);

  $des_file = camtasia_relay_file_move($xml_file_path, $newnode->camtasia_relay_presenter_name['und'][0]['value'], $newnode->camtasia_relay_profile['und'][0]['value']);

   camtasia_relay_file_attach($des_file, $filesize, $newnode);

  $xml_files = $xmldata->outputFiles->fileList->file;

  foreach ($xml_files as $xml_file) {

    $output_file_name = $xml_file['name'];

    $output_file_path = dirname($xml_file_path) ."/". $output_file_name;

    if (file_exists($output_file_path)) {

      $filesize = filesize($output_file_path);

      $des_file = camtasia_relay_file_move($output_file_path, $newnode->camtasia_relay_presenter_name['und'][0]['value'], $newnode->camtasia_relay_profile['und'][0]['value']);

      camtasia_relay_file_attach($des_file, $filesize, $newnode);
    }

  }

  //  decide whether to publish or un publish the node

  //  http://drupal.org/node/49768

  //  $node->status   unpublished/published (0|1)

  $camtasia_relay_publish = variable_get('camtasia_relay_node_publish', '');

  if ($camtasia_relay_publish==0) {

    //  publish.

    $newnode->status = 1;

  }

  else {

    //  un publish

    $newnode->status = 0;

  }



  $newnode->type = $type;

  $newnode->promote = $promote;

  return $newnode;

}

function camtasia_relay_user_check($user_name, $user_mail, $send_mail) {
  //  Check for the existence of current user $newnode->user_name
  //  Create if not exists
  //  put uid accordingly $newnode->uid
  $result = db_query("SELECT uid FROM {users} WHERE name = :name LIMIT 1", array(':name' => $user_name));
$user_id = $result->fetchField();
  if ($user_id!="") {
    //  user exists
    return $user_id;
  }   
  else {
    //  user not exists

    //  So create user

    //  send mail to user email id

    //  Drupal User module function that generates MD5 hash password

    $pass = user_password();

   // $pass = 'camtasia';

    $role_id = variable_get('camtasia_relay_user_roles', DRUPAL_AUTHENTICATED_RID);

    //$role_name = db_result(db_query('SELECT name FROM {role} WHERE rid = %d', $role_id));
 $role_name = db_query('SELECT name FROM {role} WHERE rid = :rid', array(':rid' => $role_id))->fetchField();
    //'access' => 0, /* optional, i didnt want user to be able to use this account yet*/

    $roles = array($role_id => $role_name);

    $details = array(

      'name' => $user_name,

      'pass' => $pass,

      'mail' => $user_mail,

      'status' => 1,

      'roles' => $roles,

      );

    

    $user = user_save(null, $details);

    if ($send_mail==0) {

      //	yes

      _user_mail_notify('register_admin_created', $user,  NULL);

    }

  }   //  else

  return $user->uid;  

}

function camtasia_relay_document_root() {
  $absolute_dir = dirname(__FILE__);
  $relative_dir = drupal_get_path('module', 'camtasia_relay');
  return substr($absolute_dir, 0, -1 * (1 + strlen($relative_dir)));
}


function camtasia_relay_node_view($node, $view_mode, $langcode) {
if ($node->type == 'camtasia_relay') {
$node->content['extra1'] = array(
      '#markup' => theme_camtasia_relay_media_player($node),
      '#weight' => -10,
	);
	}
}

/**
 * Takes a set of variables and generates a player from them
 * returns html code for displaying player.
 * @param string $node
 * @return $htmlCode
 */
function theme_camtasia_relay_media_player($node) {
  $html_code = "";
  $files = $node->camtasia_relay_files['und'];
  if (isset($files)) {
    foreach ($files as  $file) {
     $file_path = $file['uri'];
      $info = pathinfo($file_path);
      if ($info['extension']=="html") {
        $output = '<iframe src="'. file_create_url($file_path) .'" width="520" height="400"></iframe>';
        return $output;
      }
    }
  }
  return $html_code;
}

/**
   * check_terms function
   * returns TRUE if term is found in data base, else FALSE.
   * @param string $term_search
   * @param string $vid
   * @return TRUE/FALSE
   */
function check_terms($term_search, $vid) {
      $result = db_query("select t.* from {taxonomy_term_data} t where t.vid = :vid", array(':vid' => $vid));
	    foreach ($result as $row) {
          if ($row->name == $term_search) {
              return TRUE;
          } 
      } 
      return FALSE;
  } 
  
  /**
   * create_term function
   * Creates new term with specifid name and in specified vocabulary.
   * @param string $termname
   * @param string $vid
   */
function create_term($termname, $vid) {
      $new_term = array(
      	'name' => $termname, // or whatever you want the auto-term to be named
		'description' => '', 
      	'parent' => array(0), 
      	'vid' => $vid, );
		$term = (object) $new_term;
      taxonomy_term_save($term);
  } //function create_term($termname, $vid)

  /**
   * attach_term2node function
   * Attaches terms to node.
   * @param string $node_terms
   * @param string $newnode
   */
function attach_term2node($node_terms, $newnode) {
	foreach ($node_terms as $key => $vid_value) {
	  $vocab_object = taxonomy_vocabulary_load($vid_value);
	  $v_mac_name = $vocab_object->machine_name;
	  // check if field exists
		$field_name = 'camtasia_relay_' . $v_mac_name;
		$res = field_info_field($field_name);
		if($res == NULL){
			$field = array(
			  'field_name' => $field_name,
			  'type' => 'taxonomy_term_reference',
			  'settings' => array(
				'allowed_values' => array(
				  array(
					'vocabulary' => $vocab_object->machine_name,
					'parent' => 0
				  ),
				),
			  ),
			);

			field_create_field($field);

			$instance = array(
				'field_name' => $field_name,
				'entity_type' => 'node',
				'label' => $vocab_object->name,
				'bundle' => 'camtasia_relay',
				'required' => false,
				'widget' => array(
					'type' => 'options_select'
				),
				'display' => array(
					'default' => array('type' => 'hidden'),
					'teaser' => array('type' => 'hidden')
				)
			);
			field_create_instance($instance);
		}
	  
	  $match = check_terms($key, $vid_value);
	  if ($match != TRUE) {
		  create_term($key, $vid_value);
	  }
	  $term_id = db_query("select t.tid from {taxonomy_term_data} t where t.vid = :vid and t.name = :name LIMIT 1", array(':vid' => $vid_value, ':name' => $key, ))->fetchField();
	  if($term_id != ""){
		  $newnode->$field_name = array( 'und' =>
			array(
			  array(
				  'tid' => $term_id,
				)));
		}
	}

} 