<?php

/**
 * @file
 * Node callbacks for the camtasia_relay module.
 */

function camtasia_relay_check_sum_string($input)
{
  $sum = 458289; // Starting Checksum Seed
  $multiplier = 3;
  $input_len = strlen($input);
  for ( $i = 0; $i < $input_len; $i++ )
  {

     $sum += ord((string)$input[$i]) * $multiplier;
     $multiplier++;
  }
  return $sum;
}

function camtasia_relay_process_file($hash_checksum, $xml_file_path) {
$log_msg = "RPC call made: camtasia_relay_process_file with parameters hash_checksum: $hash_checksum, xml_file_path: $xml_file_path";
watchdog('camtasia_relay', $log_msg , NULL, WATCHDOG_ERROR, NULL);
  $flag_node_update = FALSE;
  $dirloc = variable_get('camtasia_relay_dirloc', '');
  $file_path = $dirloc."/".$xml_file_path;
  if (!is_file($file_path)) {
  $log_msg = "RPC call return: camtasia_relay_process_file File Not Exists";
watchdog('camtasia_relay', $log_msg , NULL, WATCHDOG_ERROR, NULL);
    return "File Not Exists";
  }
  $xml = file_get_contents($file_path);
  //this creates the simple xml object
  $xmldata = new simplexmlelement($xml);
  $duration = (string)$xmldata->totalDuration;
  $record_time = (string)$xmldata->date;
  $title = (string)$xmldata->title;
  $stored_secret_key = (string)variable_get('camtasia_relay_shared_secret_key', '');
  
  $validation_checksum = camtasia_relay_check_sum_string( $stored_secret_key ) . camtasia_relay_check_sum_string( $duration ) . camtasia_relay_check_sum_string( $record_time ) . camtasia_relay_check_sum_string( $title );
  if($validation_checksum != $hash_checksum){
    $log_msg = "RPC call return: camtasia_relay_process_file validation_checksum: $validation_checksum";
watchdog('camtasia_relay', $log_msg , NULL, WATCHDOG_ERROR, NULL);
    return "Access Denied";
  }

  $result = db_query('SELECT n.nid FROM {node} n WHERE n.type = '."'camtasia_relay'");
  
  while ($node = db_fetch_object($result)) {
    $node_obj = node_load($node->nid);
    global $user;
    $user->uid = 1;
    if (($node_obj->title == $xmldata->title)&&($node_obj->profile == $xmldata->profile)&&($node_obj->email == $xmldata->presenter->email)&&($node_obj->date == $xmldata->date)) {
      $filesize = filesize($file_path);
      $des_file = camtasia_relay_file_move($file_path, $xmldata->presenter->userName, $node_obj->profile);
      camtasia_relay_file_attach($des_file, $filesize, $node_obj);
      
      $xml_files = $xmldata->outputFiles->fileList->file;
      foreach ($xml_files as $xml_file) {
        $output_file_name = $xml_file['name'];
        $output_file_path = dirname($file_path) ."/". $output_file_name;
        if (file_exists($output_file_path)) {
          $filesize = filesize($output_file_path);
          $des_file = camtasia_relay_file_move($output_file_path, $xmldata->presenter->userName, $node_obj->profile);
          camtasia_relay_file_attach($des_file, $filesize, $node_obj);
        }
      }
      $flag_node_update = TRUE;
      node_save($node_obj);
      return url("node/$node_obj->nid", array('absolute' => TRUE));
    }
  }
  
  if ($flag_node_update == FALSE) {
    $newnode = camtasia_relay_node_create($xmldata, $file_path, 1, 1, 0, 'camtasia_relay');
      if ($newnode==0) {
        return "Error in creating node";
      }
      node_save($newnode);
      return url("node/$newnode->nid", array('absolute' => TRUE));
  }
   return "Error in Content Creation";
}

/**
 * camtasia_relay_node_create function
 * returns node object.
 * @param string $xmldata
 * @param string $fileName
 * @param string $uid
 * @param string $status
 * @param string $promote
 * @param string $type
 * @return $newnode
 */
function camtasia_relay_node_create($xmldata, $xml_file_path, $uid, $status, $promote, $type) {
  $newnode = new stdClass();
  $newnode->title = $xmldata->title;
  if (!empty($xmldata->description)) {
    $newnode->body = $xmldata->description;
  }
  else {
    $newnode->body = 'Description Not Provided';
  }
  $newnode->date = $xmldata->date;
  $newnode->profile = $xmldata->profile;
  $newnode->display_name = $xmldata->presenter->displayName;
  $newnode->user_name = $xmldata->presenter->userName;
  $newnode->email = $xmldata->presenter->email;
  $newnode->client_ip = $xmldata->clientInfo->clientIP;
  $newnode->client_computer_name = $xmldata->clientInfo->clientComputerName;
  $newnode->server_host_name = $xmldata->serverInfo->serverHostname;
  $newnode->encoding_preset = $xmldata->serverInfo->encodingPreset;
  $newnode->time_to_encode = $xmldata->serverInfo->timeToEncode;
  $newnode->time_in_queue = $xmldata->serverInfo->timeInQueue;
  $newnode->source_resolution = $xmldata->sourceRecording->resolution;
  $newnode->source_duration = $xmldata->sourceRecording->duration;
  $newnode->source_file = $xmldata->sourceRecording->fileList->file->attributes()->serverPath;
  $newnode->output_resolution = $xmldata->outputFiles->resolution;
  $newnode->output_duration = $xmldata->outputFiles->duration;
  $newnode->output_file = $xmldata->outputFiles->fileList->file->attributes()->destinationPath;
  $camtasia_relay_default_user = variable_get('camtasia_relay_default_user', '');
  $mail_Opt = variable_get('camtasia_relay_node_mail', '');

  if (empty($camtasia_relay_default_user)) {
    $user_name = $newnode->user_name;
  }
  else {
    $user_name = $camtasia_relay_default_user;
  }
  $uid = camtasia_relay_user_check($user_name, $newnode->email, $mail_Opt);
  global $user;
  $user = user_load($uid);
  $newnode->uid = $uid;
  $filesize = filesize($xml_file_path);
  $des_file = camtasia_relay_file_move($xml_file_path, $newnode->user_name, $newnode->profile);

  camtasia_relay_file_attach($des_file, $filesize, $newnode);
  $xml_files = $xmldata->outputFiles->fileList->file;
  foreach ($xml_files as $xml_file) {
    $output_file_name = $xml_file['name'];
    $output_file_path = dirname($xml_file_path) ."/". $output_file_name;
    if (file_exists($output_file_path)) {
      $filesize = filesize($output_file_path);
      $des_file = camtasia_relay_file_move($output_file_path, $newnode->user_name, $newnode->profile);
      camtasia_relay_file_attach($des_file, $filesize, $newnode);
    }
  }
  //  decide whether to publish or un publish the node
  //  http://drupal.org/node/49768
  //  $node->status   unpublished/published (0|1)
  $camtasia_relay_publish = variable_get('camtasia_relay_node_publish', '');
  if ($camtasia_relay_publish==0) {
    //  publish.
    $newnode->status = 1;
  }
  else {
    //  un publish
    $newnode->status = 0;
  }

  $newnode->type = $type;
  $newnode->promote = $promote;
  return $newnode;
}

function camtasia_relay_file_attach( $file_path_des, $filesize, $node_obj) {
  $name = basename($file_path_des);
  $mime = file_get_mimetype($name);
  $file = new stdClass();
  $file->filename = $name;
  $file->filepath = $file_path_des;
  $file->filemime = $mime;
  $file->filesize = $filesize;
  $file_obj->filesource = $name;

  $file->uid = $node_obj->uid;
  $file->status = FILE_STATUS_PERMANENT;
  $file->timestamp = time();
  $file->list = 1;
  $file->new = TRUE;

  drupal_write_record('files', $file);
  // Attach the file object to your node
  $err_msg = $file->fid;
  watchdog('camtasia_relay', 'file'. $err_msg , NULL, WATCHDOG_ERROR, NULL);
  $node_obj->files[$file->fid] = $file;
}

function camtasia_relay_file_move($src_file_path, $user_name, $profile) {
  $user_name = str_ireplace(' ', '_', $user_name);
  $profile = str_ireplace(' ', '_', $profile);
  $info = pathinfo($src_file_path);

  //$files_dir = $_SERVER['DOCUMENT_ROOT'] . base_path() . file_directory_path() ."/";
  $files_dir = realpath(".") . "/".file_directory_path() ."/";

  $camtasia_relay_dir = $files_dir . 'camtasia_relay1/';
  $user_dir = $camtasia_relay_dir . $user_name .'/';
  $profile_dir = $user_dir . $profile .'/';

  $ext_dir = $profile_dir;

  $des_file_path = $ext_dir . '/'. basename($src_file_path);
  camtasia_relay_mkdir_r($ext_dir);
  file_move($src_file_path, $des_file_path,  FILE_EXISTS_REPLACE);
  $relative_file_path = 'camtasia_relay1/' . $user_name .'/'. $profile . '/' . basename($src_file_path);
  return $relative_file_path;
}

function camtasia_relay_user_check($user_name, $user_mail, $send_mail) {
  //  Check for the existence of current user $newnode->user_name
  //  Create if not exists
  //  put uid accordingly $newnode->uid
  $result = db_query("SELECT * FROM {users} WHERE name = '%s'", $user_name);

  $row = db_fetch_array($result);
  if (!empty($row)) {
    //  user exists
    return $row['uid'];
  }   //  if (!empty($row)) {
  else {
    
    //  user not exists
    //  So create user
    //  send mail to user email id
    //  Drupal User module function that generates MD5 hash password
    //$pass = user_password();
    $pass = 'camtasia';
    $role_id = variable_get('camtasia_relay_user_roles', DRUPAL_AUTHENTICATED_RID);
    $role_name = db_result(db_query('SELECT name FROM {role} WHERE rid = %d', $role_id));
    //'access' => 0, /* optional, i didnt want user to be able to use this account yet*/
    $roles = array($role_id => $role_name);
    $details = array(
      'name' => $user_name,
      'pass' => $pass,
      'mail' => $user_mail,
      'status' => 1,
      'roles' => $roles,
      );
    
    $user = user_save(null, $details);
    if ($send_mail==0) {
      //	yes
      _user_mail_notify('register_admin_created', $user,  NULL);
    }
  }   //  else
  return $user->uid;  
}