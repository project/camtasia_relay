<?php
// $Id$

/**
 * @file
 * Administration page callbacks for the camtasia_relay module.
 */

/**
 * camtasia_relay_admin_settings function
 * returns form object for General Settings in camtasia_relay module.
 * @return $form
 */
function camtasia_relay_admin_settings() {
  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t("List of General Settings"),
  );
  $form['general_settings']['camtasia_relay_dirloc'] = array(
    '#type' => 'textfield',
    '#title' => t('The absolute path to your presentations'),
    '#default_value' => variable_get('camtasia_relay_dirloc', '/sites/default/files'),
    '#size' => 100,
    '#maxlength' => 300,
    '#required' => TRUE, 
    '#description' => t("the location of your camtasia_relay videos ( No trailing Slash '/' )"),
  );

  $opt_pub[0]="Publish";
  $opt_pub[1]="Un Publish";
  $form['general_settings']['camtasia_relay_node_publish'] = array(
    '#type' => 'radios',
    '#title' => t('After node creation'),
    '#default_value' => variable_get('camtasia_relay_node_publish', 0),
    '#options' => $opt_pub,
    '#description' => t("Select what to do after node creation."),
  );
  $form['general_settings']['camtasia_relay_default_user'] = array(
    '#type' => 'textfield',
    '#title' => t('The default user to create'),
    '#default_value' => variable_get('camtasia_relay_default_user', ''),
    '#size' => 100,
    '#maxlength' => 100,
    '#description' => t("Enter the Default User name or Leave empty to create node by the author."),
  );
  $opt_mail[0]="Yes";
  $opt_mail[1]="No";
  $form['general_settings']['camtasia_relay_node_mail'] = array(
    '#type' => 'radios',
    '#title' => t('Send an email when a new user is created?'),
    '#default_value' => variable_get('camtasia_relay_node_mail', 0),
    '#options' => $opt_mail,
    '#description' => t("Select what to do when a user is created."),
  );
  
  $roles = user_roles(TRUE);
  $form['general_settings']['camtasia_relay_user_roles'] = array(
    '#type' => 'radios',
    '#title' => t('Assign this role to created user'),
    '#default_value' => variable_get('camtasia_relay_user_roles', DRUPAL_AUTHENTICATED_RID),
    '#options' => $roles,
    '#description' => t("Select what role to assign when a user is created."),
  );
  
  $form['general_settings']['camtasia_relay_shared_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('The Shared Secret Key'),
    '#default_value' => variable_get('camtasia_relay_shared_secret_key', ''),
    '#size' => 100,
    '#maxlength' => 100,
    '#description' => t("Enter the Shared Secret Key"),
  );
  
    $form['vocabulary_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vocabulary Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t("List of Vocabulary Settings"),
  );
  $vocabulary = new StdClass();
  foreach (taxonomy_get_vocabularies() as $vid => $vocabulary_object) {
			$name[$vid] = $vocabulary_object->name;
  }
  	$form['vocabulary_settings']['camtasia_relay_add_terms'] = array(
        '#type' => 'checkbox',
        '#title' => t('Add terms to vocabulary'),
        '#return_value' => 1,
        '#default_value' => variable_get('camtasia_relay_add_terms', 0),
        '#description' => t("Check the box, If you want to Add terms to vocabulary.")
        );
  return system_settings_form($form);
}

function _camtasia_relay_validate_numeric_element($element, &$form_state) {
  $value = $element['#value'];

  if (!is_numeric($value)) {
    form_error($element, t('The field %name is not a valid number.', array('%name' => $element['#title'])));
  }
  elseif (isset($element['#max_value']) && $value > $element['#max_value']) {
    form_error($element, t('The field %name cannot be greater than @max.', array('%name' => $element['#title'], '@max' => $element['#max_value'])));
  }
  elseif (isset($element['#min_value']) && $value < $element['#min_value']) {
    form_error($element, t('The field %name cannot be less than @min.', array('%name' => $element['#title'], '@min' => $element['#min_value'])));
  }
}

