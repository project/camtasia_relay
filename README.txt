
-- SUMMARY --

About Camtasia camtasia_relay:
Camtasia Relay allows multiple people to create screencast videos with just a computer.
Once recorded, the video is automatically produced and available for viewing online, on an iPod or just about anywhere. (http://www.techsmith.com/camtasiacamtasia_relay.asp)

Steps:
1)A user captures media using Camtasia camtasia_relay client.
2)This client will upload the media to Camtasia camtasia_relay server.
3)The server converts this media to different formats like flv, ipod.... using pre configured encode settings.
4)The server publishes this media to Drupal server via ftp.
5)The Drupal module will scan the directory on cron run and create nodes for each recorded media.

The working of this module can be seen at http://www.lyceumtechnologies.com/
Username: demo Password: demo

-- REQUIREMENTS --

* upload module(CORE)and

* views module(CONTRIBUTED)


-- INSTALLATION --

* see INSTALL.txt


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  camtasia_relay module:


-- CONTACT --

Current maintainers:
* Nikhil Dubbaka (TechNikh) - http://drupal.org/user/372123


This project has been sponsored by:
* TechSmith
  TechSmith provides more than 30 countries with screen capture and recording 
  software for individual and professional use. Founded in 1987 by William 
  Hamilton, who remains the company�s president today, TechSmith has seven 
  products that do anything from take screen captures, to screen recording 
  and managing consumer content. Visit http://www.techsmith.com/ for more 
  information.