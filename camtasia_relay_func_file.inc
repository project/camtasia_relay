<?php

/**
 * @file
 * File callbacks for the camtasia_relay module.
 */

/**
 * camtasia_relay_mkdir_r function
 * creates directories recursively
 * @param string $dirName
 * @param string $rights
 */
function camtasia_relay_mkdir_r($dir_name, $rights=0777) {
  $dirs = explode('/', $dir_name);
  $dir='';
  foreach ($dirs as $part) {
    $dir .=$part . '/';
    if (!is_dir($dir) && drupal_strlen($dir)>0)
      mkdir($dir, $rights);
  }
}
